const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');
const TasksService = require('../../model/TaskService');

const app = express();
const tasksService = new TasksService();

app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', path.join(__dirname.split("/").slice(0, -2).join("/"), "views"));
app.set('view engine', "ejs");
app.use(express.static(path.join(__dirname.split("/").slice(0, -2).join("/"), 'assets')));

app.post('/add-task', async (req, res) => {
    const taskName = req.body.task;
    await tasksService.addTask(taskName)
    return res.render("index",{...tasksService.copyOfParams, showTodoPage: true, tasks: tasksService.showNotDoneTasks})
});

app.get('/', (req, res) => {
    res.render('index', {...tasksService.params, showTodoPage: true, tasks: tasksService.showNotDoneTasks})
});

app.get('/done', (req, res) => {
    res.render('index', {...tasksService.params, showTodoPage: false, tasks: tasksService.showDoneTasks})
});

app.post('/api/tasks/:taskName/done', (req, res) => {
    const taskName = req.params.taskName;
    tasksService.deleteTask(taskName)
    res.render('index', {...tasksService.params, showTodoPage: false, tasks: tasksService.showDoneTasks})
});

module.exports = app;
