const makeStoppable = require("stoppable");
const http = require("http");
const routes = require('./routes/routes');

const server = makeStoppable(http.createServer(routes));

const stopServer = () => {
  return new Promise((resolve) => {
    server.stop(resolve);
  });
};

module.exports = () => {
  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
};
