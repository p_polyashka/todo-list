class TasksService {
    constructor() {
        this.tasks = { notDone: [], done: []};
        this.params = {
            isShort: false,
            isAlreadyExist: "",
            showAddedTask: false,
        }
        this.copyOfParams = {
            ...this.params
        }
    }

    async addTask(taskName) {
        if (this.#paramsIsAlreadyExist(taskName)) {
            this.copyOfParams.isAlreadyExist = taskName;
        }
        else if (this.#paramsIsShort(taskName)) {
            this.copyOfParams.isShort = true;
        }
        else {
            this.#paramsShowAddedTask(taskName);
            this.copyOfParams.showAddedTask = true;
        }
    }

    async deleteTask(taskName) {
        this.tasks.notDone = this.tasks.notDone.filter((item) => item !== taskName);
        this.tasks.done.push(taskName);
    }
    #paramsIsAlreadyExist(taskName) {
        this.#updateParams();
        return this.tasks.done.includes(taskName) || this.tasks.notDone.includes(taskName);
    }

    #paramsShowAddedTask(taskName) {
        this.#updateParams();
        this.tasks.notDone.push(taskName);
    }

    #paramsIsShort(taskName) {
        return (taskName.length < 3);
    }

    #updateParams() {
        this.copyOfParams = {
            ...this.params
        }
    }

    get showDoneTasks() {
        return this.tasks.done;
    }
    get showNotDoneTasks() {
        return this.tasks.notDone;
    }

}
module.exports = TasksService;